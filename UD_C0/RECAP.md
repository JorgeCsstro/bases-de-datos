# Unidad C0: Recapitulación

Jorge Castro Tramoyeres 1ºDAW

* [[ENLACE REPO](https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C0)]

# Introducción

Las bases de datos funcionan mediante el lenguaje llamado SQL (Structured Query Language), las bases de datos se utilizan para poder trabajar con una gran cantidad de datos a la misma vez con Tablas, Campos y Registros para poder ordenarlos, en una base de datos (que sea relacional) pueden relacionarse varias tablas entre sí mediante claves primarias y claves foraneas.

En una base de datos hay muchas maneras de manipular los datos mediante comandos, hay diferentes tipos de comandos (DDL, DML, DCL, TCL) que hablaremos de cada uno de ellos más adelante.

Para poder interactuar con tu base de datos hay softwares que pueden interactuar con ella, llamados DBMS.

También veremos un poco como funciona el modelo cliente-servidor y porque es muy útil con las bases de datos.

![](https://i.ibb.co/Pj5xgsZ/Base-de-Datos-YMANT.jpg)

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Una base de datos se encarga de  ordenar datos de la manera más comoda posible y trabajar con una gran cantidad de datos. Se pueden almacenar muchos tipos de datos como, nombres, direcciones, correos electronicos, etc.

De esta forma reusuelve varios problemas como: modificar, añadir, eliminar y acceder a los difrentes datos de una forma eficiente y cómoda. También un problema bastante importante que resuelve es que los datos pueden interactuar entre sí.

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

Los DBMS es el software que se encarga de interactuar con la base de datos, normalmente estos softwares utilizan el lenguaje .sql

El DBMS gestiona tres cosas importantes:
* Los datos.
* Permite acceder a los datos, bloquearlos y modificarlos.
* El esquema de la base de datos, que define la estructura lógica de la base de datos. 

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* __Oracle DB__     -> Se usa, No software libre, Usa cliente-servidor.
* __IMB Db2__       -> Creo que no se usa, No software libre, Usa cliente-servidor.
* __SQLite__        -> Se usa, Software libre, No usa cliente-servidor.
* __MariaDB__       -> Se usa, Software libre, Usa cliente-servidor.
* __SQL Server__    -> Se usa, No software libre, Usa cliente-servidor.
* __PostgreSQL__    -> Se usa, No software libre, Usa cliente-servidor.
* __mySQL__         -> Se usa, Software libre, Usa cliente-servidor.

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

Es interesante porque siempre tendrás acceso a los datos estés donde estes y asi tambien si eres una empresa todos podrán acceder a esa base de datos.

* __Cliente__: Es la maquina que quiere interactuar con el Servidor.
* __Servidor__: Es el proveedor de recursos (en este caso datos), este escucha a las peticiones del cliente y interactua a partir de sus peticiones.
* __Red__: Es la forma en la cual el cliente y servidor van a poder interactuar entre si (por cable, ondas, etc).
* __Puerto de escucha__: Es el puerto en el cual el servidor va poder escuchar al cliente.
* __Petición__: El cliente manda un mensaje al servidor de que quiere hacer en el servidor (sacar datos, incorporar, modificar, etc).
* __Respuesta__: El servidor manda un mensaje al cliente según la petición realizada.

## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?

SQL es un lenguaje que ayuda a estructurar datos a partir de "Queries" (Que por lo que tengo entendido son peticiones).

### Instrucciones de SQL

#### DDL
Data Definition Lenguaje, es un conjunto de comandos en SQL utilizados para definir y modificar la __ESTRUCTURA__ de las bases de datos y sus objetos. Por ejemplo (CREATE TABLE, ALTER TABLE, etc).
#### DML
Data Manipulation Lenguaje, es un conjunto de comandos en SQL utilizados para insertar, modificar, borrar y consultar __DATOS__ en las bases de datos. Por ejemplo (SELECT, INSERT, etc).
#### DCL
Data Control Lenguaje, es un conjunto de comandos en SQL que se utilizan para __CONTROLAR EL ACCESO__ a los datos en las bases de datos. Por ejemplo (GRANT, REVOKE).
#### TCL
Transaction Control Lenguaje, es un conjunto de comandos que se utilizan para manejar las __TRANSACCIONES__ en la base de datos. Esto ayuda a la base de datos a mantener la integridad de los datos, asi si ha habido algún error (se te ha apagado la luz en tu casa, se ha mojado tu dico duro JUSTAMENTE cuando hacias una transacción, etc). Por ejemplo (COMMIT, ROLLBACK, SAVEPOINT, etc).

## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

Una base de datos relacional es una base de datos que puede relacionar datos entre sí.

Las ventajas principales son:

* Tener una estructura organizada.
* Que los datos puedan interactuar entre sí mediante claves foraneas y claves principales.

* __Relación (tabla)__: Es la estructura por defecto de las bases de datos que se divide por campos y registros.
* __Atributo/Campo (columna)__: Es el indentifiador de los datos para poder saber a que se refiere cada dato (Ej: HOSPITALES).
* __Registro/Tupla (fila)__: Tiene datos concretos para cada uno de los campos (columnas)(Ej: "Hospital Peset").

# Referencias
* [[WIKIPEDIA](https://es.wikipedia.org/)]
* [[Datademia(SQL)](https://datademia.es/blog/que-es-sql)]
