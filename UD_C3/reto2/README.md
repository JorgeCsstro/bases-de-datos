# Reto 3.2 DDL - Control de acceso

## Autor: Jorge Castro

En este reto vamos a ver los diferentes comandos de mysql para administrar usuarios y ver todas las opciones que nos ofrece 

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C3/reto2?ref_type=heads

## Crear/Modificar/Eliminar usuarios

### Crear usuario

En este apartado creamos un usuario llamado `isaac` y que solo se pueda conectar por el puerto de `localhost(127.0.0.1)` y con la contraseña `isaac`.

```sql
CREATE USER 'isaac'@'localhost' IDENTIFIED BY 'isaac';
```

### Modificar Usuarios

Para modificar un usuario (contraseña, nombre, etc) se usa el comando `ALTER USER`.

```sql
ALTER USER 'isaac'@'localhost' IDENTIFIED BY 'nueva_contraseña';
```

### Eliminar Usuarios

Para eliminar un usuario utilizamos el comando `DROP USER`.

```sql
DROP USER 'isaac'@'localhost';
```

## Autenticación de Usuarios

- **Autenticación por contraseña**: Se utiliza la contraseña para ese usuario (es el más basico)

- **Autenticación por plugins**: Hay plugins que pueden hacer la autenticacion mas segura como `mysql_native_password`.

Esta se puede utilizar de la siguiente manera:

```sql
CREATE USER 'usuario_plugin'@'localhost' IDENTIFIED WITH 'mysql_native_password' BY 'isaac';
```

## Mostrar Usuarios Existentes y Sus Permisos

Para mostrar los usuarios existentes en la base de datos, se pueden mirar en la tabla `mysql.user`.

```sql
SELECT user, host FROM mysql.user;
```

Para mostrar los permisos de un usuario, se utiliza el comando `SHOW GRANTS`.

```sql
SHOW GRANTS FOR 'isaac'@'localhost';
```

## Permisos y Granularidad

### Tipos de Permisos

Los permisos en MySQL se pueden otorgar a nivel de servidor, base de datos, tabla, columna, procedimiento, y otros niveles.
La granuladidad en mysql permite que cada usuario tenga permisos especificos para que por ejemplo un usuario solo pueda selecionar en la tabla Ventas por ejemplo

- `ALL PRIVILEGES`: Otorga todos los permisos, basicamente para actuar como root.
- `SELECT`, `INSERT`, `UPDATE`, `DELETE`: Permisos para manipular base de datos.
- `CREATE`, `DROP`: Permisos para la creación y eliminación de bases de datos y tablas.
- `GRANT OPTION`: Permiso para otorgar permisos a otros usuarios.

### Otorgar y revocar permisos

Para **OTORGAR** permisos, se utiliza el comando `GRANT`:

```sql
GRANT SELECT, INSERT ON base_de_datos.tabla TO 'isaac'@'localhost';
```

Para **REVOCAR** permisos, se usa el comando `REVOKE`:

```sql
REVOKE INSERT ON base_de_datos.tabla FROM 'isaac'@'localhost';
```

## Permisos Necesarios para Gestionar Usuarios

Para gestionar usuarios y sus permisos, normalmente se necesita el permiso `GRANT OPTION` y permisos administrativos como `CREATE USER`, `DROP USER`, `GRANT`, `REVOKE`.

## Agrupar Usuarios (Grupos/Roles)

Los roles son colecciones de permisos que se pueden asignar a los usuarios. Esto facilita la gestión de permisos para grupos de usuarios.

### Crear un Rol

```sql
CREATE ROLE 'nombre_rol'@'%';
```

### Ver Roles
Los roles en mysql estan en la misma tabla que los usuarios, asi que se haría el mismo comando que el apartado anterior `## Mostrar Usuarios Existentes y Sus Permisos`.

### Asignar permisos a un Rol

```sql
GRANT SELECT, INSERT ON base_de_datos.tabla TO 'nombre_rol'@'%';
```

### Añadir un rol a un usuario

```sql
GRANT 'nombre_rol'@'%' TO 'isaac'@'localhost';
```

### Activar un Rol

Se pueden activar varios roles para el usuario

```sql
SET ROLE 'nombre_rol'@'%';
SET ROLE 'nombre_rol'@'%', 'nombre_rol_2'@'%';
```

## Comandos para Gestionar Usuarios y Sus Permisos

Aqui tenemos un pequeño repaso de todos los puntos vistos anteriormente sobre la gestion de los usuarios y roles de la base de datos.

- `CREATE USER`: Crear un nuevo usuario.
- `ALTER USER`: Modificar un usuario existente.
- `DROP USER`: Eliminar un usuario.
- `GRANT`: Otorgar permisos.
- `REVOKE`: Revocar permisos.
- `SHOW GRANTS`: Mostrar los permisos otorgados a un usuario.
- `CREATE ROLE`: Crear un rol.
- `GRANT ... TO ...`: Asignar un rol a un usuario.
- `SET ROLE`: Activar un rol.

## COSAS (algo) IMPORTANTES A AÑADIR

Los roles y usuarios se indentifican por el nombre y el host. ES DECIR, si tengo un usuario `hola` con el host `%` y tiene permisos de `INSERT` en la BBDD `ALOO`, NO ES LO MISMO que un usuario `hola` con el host `localhost` que tiene permisos de `SELECT` en la BBDD `ALOO`.