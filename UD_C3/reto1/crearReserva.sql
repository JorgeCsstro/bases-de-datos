CREATE TABLE `Aviones`.`Reservas` (
  `idReservas` INT NOT NULL,
  `id_vuelo` INT NOT NULL,
  `id_pasajero` INT NOT NULL,
  `n_asiento` CHAR(3) NOT NULL,
  PRIMARY KEY (`idReservas`),
  UNIQUE INDEX `idReservas_UNIQUE` (`idReservas` ASC) VISIBLE,
  UNIQUE INDEX `id_vuelo_UNIQUE` (`id_vuelo` ASC) VISIBLE,
  UNIQUE INDEX `id_pasajero_UNIQUE` (`id_pasajero` ASC) VISIBLE,
  CONSTRAINT `idvuelo`
    FOREIGN KEY (`id_vuelo`)
    REFERENCES `Aviones`.`Vuelos` (`id_vuelo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idpasajero`
    FOREIGN KEY (`id_pasajero`)
    REFERENCES `Aviones`.`Pasajeros` (`id_pasajero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
