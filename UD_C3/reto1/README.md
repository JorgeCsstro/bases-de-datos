# Reto 3.1 DDL - Definición de datos en MySQL
**[Bases de Datos] Unidad Didáctica 3: DDL - Definición de datos en MySQL**

## Autor: Jorge Castro

Para este reto vamos a crear una base de datos llamada `Aviones` y documentar lo que hace cada sentencia y tambien ver como funciona el `CLI de mysql`

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C3/reto1?ref_type=heads

## Conexion con mysql CLI

### Conectarse desde CLI

Nos Conectamos mediante CLI con esta sentencia
```bash
mysql -u root -p -h 127.0.0.1 -P 55006
#(poner despues: bdrootpass)
```

### Comandos utiles (CLI)

#### Mostrar BBDD

Estos comandos nos mostrarán todas las BBDD en esa conexion (Hacen lo mismo)

```sql
SHOW DATABASES;
SHOW SCHEMAS;
```

#### Muestra todas las tablas de una base de datos

```sql
SHOW TABLES FROM Chinook;
SHOW TABLES; --(si hay predeterminada)
```

#### Muestra los tipos de datos de los campos de una tabla

```sql
DESCRIBE Chinook.Invoice;
-- Es mejor DESCRIBE porque es mas sencilla
SHOW COLUMNS FROM Invoice;
```

#### Poner un Schema como predeterminada

```sql
USE Chinook;
```

#### Mirar el Schema que se esta utilizando (predeterminado)

```sql
SELECT DATABASE();
```

#### Muestra si esta activado o no el autocommit

```sql
SELECT @@autocommit;
```

#### Cambiar el estado de autocommit

```sql
SET autocommit=0;
```

### [ Con autocommit=0 ]

#### Hacer un checkpoint de el estado de la query para decir que todo esta bien

```sql
COMMIT;
```

#### Deshacer todos los cambios hasta el ultimo COMMIT

```sql
ROLLBACK;
```

#### Para que sirven?
El comando `COMMIT` sirve para poder hacer cambios temporales en la base de datos y si no nos gustan los cambios en ella podemos hacer un `ROLLBACK` para deshacerlos

## Creacion de `Aviones`

#### Creacion Schema si no existe

```sql
CREATE SCHEMA IF NOT EXISTS `Aviones`;
```

#### Creacion tabla `Pasajeros`

```sql
CREATE TABLE `Pasajeros` (
  `id_pasajero` int NOT NULL AUTO_INCREMENT,
  `numero_pasaporte` int DEFAULT NULL,
  `nombre_pasajero` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_pasajero`),
  UNIQUE KEY `idPasajero_UNIQUE` (`id_pasajero`),
  UNIQUE KEY `num_pasa_UNIQUE` (`numero_pasaporte`)
)
```

#### Creacion tabla `Vuelos`

```sql
CREATE TABLE `Vuelos` (
  `id_vuelo` int NOT NULL AUTO_INCREMENT,
  `origen` varchar(45) DEFAULT NULL,
  `destino` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`id_vuelo`),
  UNIQUE KEY `idVuelo_UNIQUE` (`id_vuelo`)
)
```

#### Creacion tabla `Reservas`

```sql
CREATE TABLE `Reservas` (
  `idReservas` int NOT NULL,
  `id_vuelo` int NOT NULL,
  `id_pasajero` int NOT NULL,
  `n_asiento` char(3) NOT NULL,
  PRIMARY KEY (`idReservas`),
  UNIQUE KEY `idReservas_UNIQUE` (`idReservas`),
  UNIQUE KEY `id_vuelo_UNIQUE` (`id_vuelo`),
  UNIQUE KEY `id_pasajero_UNIQUE` (`id_pasajero`),
  CONSTRAINT `idpasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`),
  CONSTRAINT `idvuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)
)
```

### Tipos de datos
`Char`: Un caracter (A,m,F,etc).

`Varchar()`: Para hacer una cadena de char (caracteres) y hacer asi una frase o palabra con esta.

`INT`: Para definir que el dato es un numero entero sin decimales.

`DATE`: Para definir que el dato tiene que ser una fecha YYYY-MM-DD.

### Preguntas

#### Porque 3 tablas?

Añadimos `Reservas` como intermedario entre `Vuelos` y `Pasajeros`, porque tenemos que hacer una relacion de N:M.

#### Cuales son las claves primarias y foráneas?

##### Primarias

Son aquellas por las que la tabla se va a indentificar en si, normalmente son IDs, como por ejemplo, en la tabla `Vuelos`:

```sql
`id_vuelo` int NOT NULL AUTO_INCREMENT,
PRIMARY KEY (`id_vuelo`),
UNIQUE KEY `idVuelo_UNIQUE` (`id_vuelo`)
```

Aqui le decimos que sea un `INT`, con `NOT NULL` para no se pueda dejar en null, que se incremente automaticamente, con `AUTO_INCREMENT`, para que no tengamos que ponerlo y vaya aumentando automaticamente, que sea `UNIQUE` para que no se puedan repetir y obiamente la definimos con `PRIMARY KEY` para que la linea de la tabla se indentifique por este dato

##### Foraneas

Son aquellas que cogen la referencia de otra tabla normalmente se suele poner el ID de la tabla a la que vas a referenciar, en este ejemplo la tabla `Reservas` tiene claves foraneas:

```sql
CONSTRAINT `idpasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`),
CONSTRAINT `idvuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)
```

Agui lo que hacemos es que definimos un campo con `FOREIGN KEY (id_pasajero)` y le decimos que coja como referencia otro campo de otra tabla con `Pasajeros` (`id_pasajero`).

#### Por qué utilizamos las restricciones que hemos definido y no otras? Qué restricciones son útiles en cada caso?

Obiamente las mas utiles son las `PRIMARY KEY` para que esa tabla tenga una forma de indentificarse

Después, por ejemplo para que no se puedan asignar 2 pasajeros en el mismo asiento utilizamos la restriccion `UNIQUE`.

Y las ultimas más importantes son las `NOT NULL` para que si o si el id o otros datos que necesitamos siempre tengan que estar añadidos y no vacios.


