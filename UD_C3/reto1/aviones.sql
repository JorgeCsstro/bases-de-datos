CREATE DATABASE  IF NOT EXISTS `Aviones`;
USE `Aviones`;

DROP TABLE IF EXISTS `Pasajeros`;

CREATE TABLE `Pasajeros` (
  `id_pasajero` int NOT NULL AUTO_INCREMENT,
  `numero_pasaporte` int DEFAULT NULL,
  `nombre_pasajero` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_pasajero`),
  UNIQUE KEY `idPasajero_UNIQUE` (`id_pasajero`),
  UNIQUE KEY `num_pasa_UNIQUE` (`numero_pasaporte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `Pasajeros` WRITE;
UNLOCK TABLES;

DROP TABLE IF EXISTS `Reservas`;
CREATE TABLE `Reservas` (
  `idReservas` int NOT NULL,
  `id_vuelo` int NOT NULL,
  `id_pasajero` int NOT NULL,
  `n_asiento` char(3) NOT NULL,
  PRIMARY KEY (`idReservas`),
  UNIQUE KEY `idReservas_UNIQUE` (`idReservas`),
  UNIQUE KEY `id_vuelo_UNIQUE` (`id_vuelo`),
  UNIQUE KEY `id_pasajero_UNIQUE` (`id_pasajero`),
  CONSTRAINT `idpasajero` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`),
  CONSTRAINT `idvuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `Reservas` WRITE;
UNLOCK TABLES;

DROP TABLE IF EXISTS `Vuelos`;
CREATE TABLE `Vuelos` (
  `id_vuelo` int NOT NULL AUTO_INCREMENT,
  `origen` varchar(45) DEFAULT NULL,
  `destino` varchar(45) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`id_vuelo`),
  UNIQUE KEY `idVuelo_UNIQUE` (`id_vuelo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `Vuelos` WRITE;
UNLOCK TABLES;