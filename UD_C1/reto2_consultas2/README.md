# Reto 2: Consultas básicas II

### Autor: Jorge Castro

En este reto trabajamos con la base de datos `empresa` y `videoclub`, que nos viene dada en los ficheros `empresa.sql` y `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C1/reto2_consultas2?ref_type=heads

## Qempresa 1
En esta query he realizado una consulta de la tabla `PRODUCTE` y seleccionaremos estas columnas `PROD_NUM`, y `DESCRIPCIO` para que solo nos muestren estas columnas.

(Opcional): He renombrado algunas de las columnas con `AS`.

```sql
SELECT PROD_NUM AS "Código", DESCRIPCIO
FROM PRODUCTE;
```

## Qempresa2
En esta query he reutilizado el código de la Qempresa1 y he añadido un `LIKE` para que solo coja todas las filas de la columna `DESCRIPCIO` que contengan `TENNIS`.

(Opcional): He renombrado algunas de las columnas con `AS`.

```sql
SELECT PROD_NUM AS "Código", DESCRIPCIO
FROM PRODUCTE
WHERE DESCRIPCIO LIKE "%TENNIS%";
```

## Qempresa3
En esta query he realizado una consulta de la tabla `CLIENT` y que coja el `CLIENT_COD`, `NOM`, `AREA` y `TELEFON`.

(Opcional): He renombrado algunas de las columnas con `AS`.

```sql
SELECT CLIENT_COD AS "Código", NOM, AREA, TELEFON
FROM CLIENT;
```

## Qempresa4
En esta query he reutilizado el código de la Qempresa3 y he añadido que si el `AREA` NO es `636` lo seleccione.

(Opcional): He renombrado algunas de las columnas con `AS`.

```sql
SELECT CLIENT_COD AS "Código", NOM, CIUTAT
FROM CLIENT
WHERE AREA != 636;
```

## Qempresa5
En esta query he realizado una consulta de la tabla `COMANDA`, donde seleccionamos `CLIENT_COD`, `COM_DATA` y `DATA_TRAMESA`.

(Opcional): He renombrado algunas de las columnas con `AS`.

```sql
SELECT CLIENT_COD AS "Código", COM_DATA AS "F. orden", DATA_TRAMESA AS "F. envío"
FROM COMANDA;
```

## Qvideo6
En esta query he realizado una consulta de la tabla `CLIENT`, y he seleccionado solo `Nom` y `Telefon`.

```sql
SELECT Nom, Telefon
FROM CLIENT;
```

## Qvideo7
En esta query he realizado una consulta de la tabla `FACTURA`, y he seleccionado solo `Data` y `Import`.

```sql
SELECT Data, Import
FROM FACTURA;
```

## Qvideo8
En esta query he realizado una consulta de la tabla `DETALLFACTURA`, y he seleccionado solo `Descripcio` que tengan el `CodiFactura` que sea `3`.

```sql
SELECT Descripcio
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

## Qvideo9
En esta query he realizado una consulta de la tabla `FACTURA`, y he ordenado esta por el `Import` de forma descendiente.

```sql
SELECT Descripcio
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

## Qvideo10
En esta query he seleccionado de la tabla `ACTOR` y he añadido que solo seleccione los que tienen una `X` al principio en la columna `Nom`.

```sql
SELECT *
FROM ACTOR
WHERE Nom LIKE "X%";
```
