SELECT P.Titol AS "Nombre película", G.Descripcio AS "Nombre género"
FROM PELICULA AS P
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;

-- Se pone solo una tabla en el "FROM" y despues se añade otra en el "JOIN",-- 
-- los filtras con la clave foránea y la clave principal (Clave principal siendo la del "JOIN" y foranea la que tenga la tabla del "FROM")
