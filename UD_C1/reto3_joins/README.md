# Reto 3: Consultas básicas con JOIN

### Autor: Jorge Castro

En este reto trabajamos con la base de datos `videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C1/reto3_joins?ref_type=heads

## Query1
En esta query he realizado una consulta de la tabla `PELICULA`, después he añadido con un `JOIN` la tabla `GENERE` que los filtre por los `CodiGenere` y seleccionaremos estas columnas `P.Titol`, y `G.Descripcio` para que solo nos muestren estas columnas.

(Opcional): He renombrado algunas de las columnas con `AS`.

```sql
SELECT P.Titol AS "Nombre película", G.Descripcio AS "Nombre género"
FROM PELICULA AS P
JOIN GENERE AS G
ON P.CodiGenere = G.CodiGenere;
```

## Query2
En esta query he realizado una consulta de la tabla `CLIENT`, después he añadido con un `JOIN` la tabla `FACTURA` que los filtre por los `DNI` y por los que el `C.Nom` tenga `Maria`, después solo seleccionamos toda la tabla de `CLIENT` y de `FACTURA` todo menos el `DNI`.

```sql
SELECT C.*, F.CodiFactura, F.Data, F.Import
FROM CLIENT AS C
JOIN FACTURA AS F
ON F.DNI = C.DNI
WHERE C.Nom LIKE "Maria%";
```

## Query3
En esta query he realizado una consulta de la tabla `PELICULA`, después he añadido con un `JOIN` la tabla `ACTOR` que los filtre por los `CodiActor` y seleccionaremos toda la tabla `PELICULA` y de la tabla `ACTOR`, solo el `Nom`.

```sql
SELECT P.*, A.Nom
FROM PELICULA AS P
JOIN ACTOR AS A
ON A.CodiActor = P.CodiActor;
```

## Query4
En esta query he realizado una consulta de la tabla `PELICULA`, después he añadido con un `JOIN` la tabla `INTERPRETADA` y `ACTOR` que los filtre por los `CodiPeli` (de `PELICULA` y `INTERPRETADA`) y por los que el `CodiActor` (de `INTERPRETADA` y `ACTOR`), después solo seleccionamos toda la tabla de `PELICULA`, menos `CodiActor` y de `INTERPRETADA` solo el `CodiActor` para que si hay 2 o más actores en una película me muestre los 2 o más sin que se repitan.

(Opcional): He ordenado para mejor visibilidad con `ORDER BY`.

```sql
SELECT P.CodiPeli,P.Titol,P.CodiGenere, P.SegonaPart, I.CodiActor
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON P.CodiPeli = I.CodiPeli
AND I.CodiActor = A.CodiActor
ORDER BY P.CodiPeli ASC;
```

## Query5
En esta query he realizado una consulta de la tabla `PELICULA`, después he añadido con un `JOIN` OTRA VEZ la tabla `PELICULA`, después filtro el `SegonaPart` y `CodiPeli`, después muestro el `CodiPeli` y `Titol` de la tabla añadida por `JOIN` y el `CodiPeli` y `Titol` de la tabla añadida por `FROM`.

(Opcional): He ordenado para mejor visibilidad con `ORDER BY`.

```sql
SELECT PE.CodiPeli, PE.Titol, P.CodiPeli, P.Titol
FROM PELICULA AS P
JOIN PELICULA AS PE
ON P.SegonaPart = PE.CodiPeli
ORDER BY P.CodiPeli ASC;
```