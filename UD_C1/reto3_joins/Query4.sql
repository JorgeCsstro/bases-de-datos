SELECT P.CodiPeli,P.Titol,P.CodiGenere, P.SegonaPart, I.CodiActor
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON P.CodiPeli = I.CodiPeli
AND I.CodiActor = A.CodiActor
ORDER BY P.CodiPeli ASC;
-- He ordenado para mejor visibilidad