SELECT PE.CodiPeli, PE.Titol, P.CodiPeli, P.Titol
FROM PELICULA AS P
JOIN PELICULA AS PE
ON P.SegonaPart = PE.CodiPeli
ORDER BY P.CodiPeli ASC;
-- He ordenado para mejor visibilidad