-- FROM is Left , JOIN is Right, se utiliza cuando hay cosas no asociadas
SELECT P.*, A.Nom
FROM PELICULA AS P
JOIN ACTOR AS A
ON A.CodiActor = P.CodiActor;