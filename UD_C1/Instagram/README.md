# Consultas Instagram

### Autor: Jorge Castro

En este reto trabajamos con la base de datos `instagram_low_cost`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C1/Instagram?ref_type=heads

# Query1
En esta query he realizado una consulta de las tablas `fotos` y `usuarios`, después filtramos donde el `idUsuario` sea igual en ambas tablas y que el `idUsuario` sea 36, finalmente seleccionaremos estas columnas `idUsuario`, y `idFoto` para que solo nos muestren estas columnas.

```sql
USE instagram_low_cost;
SELECT usuarios.idUsuario ,idFoto
FROM fotos,usuarios
WHERE fotos.idUsuario = usuarios.idUsuario
	AND fotos.idUsuario = 36;
```

# Query2
En esta query he realizado una consulta de las tablas `fotos` y `usuarios`, es la misma consulta que antes pero ahora he añadido `YEAR` y `MONTH` para que solo muestre aquellas de Enero de 2024.

```sql
USE instagram_low_cost;
SELECT usuarios.nombre, fotos.url, fotos.fechaCreacion
From fotos,usuarios
WHERE fotos.idUsuario = usuarios.idUsuario
    AND fotos.idUsuario = 36
    AND year(fotos.fechaCreacion) = 2024
    AND MONTH(fotos.fechaCreacion) = 01;
```

# Query3
En esta query he realizado una consulta de las tablas `fotos`, `usuarios`, `comentarios` y `comentariosFotos`, donde el `idUsuario` de la tabla `fotos` es 11 Y el `idUsuario` de la tabla `usuarios` es 36, posteriormente añadimos la tabla `comentarios` y le decimos que lo filtre por lo que su `idUsuario` sea igual al `idUsuario` de la tabla `usuarios`, finalmente añadimos la tabla `comentariosFotos` donde el `idFoto` sea igual al `idFoto` de la tabla `fotos` y el `idComentario` sea igual al `idComentario` de la tabla `comentarios`.

(He actualizado la consulta y no es igual que la descripción)

```sql
USE instagram_low_cost;
SELECT c.idUsuario,c.comentario, f.idFoto, f.idUsuario
FROM fotos f
JOIN comentarios c
	ON c.idUsuario = 36
JOIN comentariosFotos cf
	ON cf.idFoto = f.idFoto
    AND c.idComentario = cf.idComentario
WHERE
	f.idUsuario = 11
    AND f.idFoto = 12
;
```

# Query4
En esta query he realizado una consulta de las tablas `fotos`, `reaccionesFotos` y `tiposReaccion`, he cogido las reacciones con el `idUsuario` 25 y las he filtrado por el `idtipoReaccion` 4 que es el de sorprendido

```sql
USE instagram_low_cost;
SELECT rf.idUsuario, f.idFoto, f.descripcion, f.fechaCreacion, tr.descripcion
FROM reaccionesFotos rf, fotos f, tiposReaccion tr
WHERE reaccionesFotos.idUsuario = 25
    AND reaccionesFotos.idFoto = fotos.idFoto
    AND reaccionesFotos.idTipoReaccion = 4
    AND reaccionesFotos.idTipoReaccion = tiposReaccion.idTipoReaccion;
```

# Query5
En esta query he realizado una consulta de la tabla `usuarios`, he añadido la tabla `roles` donde su `idRol` sea igual al `idRol` del a tabla `usuarios`, después he añadido la tabla `reaccionesComentarios` donde el `idUsuario` sea igual al `idUsuario` del a tabla `usuarios`, posteriormente he añadido la tabla `tiposReaccion` donde el `idTipoReaccion` sea igual al `idTipoReaccion` del a tabla `reaccionesComentarios`, después he filtrado donde la `descripcion` de la tabla `roles` sea `Administrador` y la `descripcion` de la tabla `tiposReaccion` sea `Me gusta`, finalmente he agrupado todo por el `idUsuario` de la tabla `usuarios` donde tengan un `totalMegusta` mayor que 2.

```sql
USE instagram_low_cost;
SELECT u.idUsuario, u.nombre AS nombreUsuario, COUNT(rc.idTipoReaccion) AS totalMeGusta
FROM usuarios u 
JOIN roles r 
    ON u.idRol = r.idRol 
JOIN reaccionesComentarios rc
    ON u.idUsuario = rc.idUsuario
JOIN tiposReaccion tr
    ON rc.idTipoReaccion = tr.idTipoReaccion
WHERE r.descripcion = 'Administrador'
    AND tr.descripcion = 'Me gusta'
GROUP BY u.idUsuario HAVING totalMeGusta > 2;
```

# Query6
En esta query he realizado una consulta de la tabla `reaccionesFotos`, he añadido la tabla `tiposReaccion` donde su `idTipoReacion` sea igual al `idTipoReacion` del a tabla `reaccionesFotos`, después he filtrado donde el `idUsuario` de la tabla `reaccionesFotos` sea 45, tambien `idFoto` sea igual a 12 y su `descripcion` sea `Me divierte`, y para acabar he contado todo y lo he renombrado a `totalMeDivierte`

```sql
USE instagram_low_cost;
SELECT COUNT(*) AS totalMeDivierte 
FROM reaccionesFotos rf
JOIN tiposReaccion tr
    ON rf.idTipoReaccion = tr.idTipoReaccion 
WHERE rf.idUsuario = 45
    AND rf.idFoto = 12
    AND tr.descripcion = 'Me divierte';
```

# Query7
n esta query he realizado una consulta de la tabla `totos`, donde su `descripcion` contiene la palabra `playa` para ello he utilizado los `%`, despues he contado las `descripcion` y renombrado a `Contador`.

```sql
USE instagram_low_cost;
SELECT COUNT(descripcion) AS "Contador"
FROM fotos
WHERE descripcion LIKE "%playa%";
```