USE instagram_low_cost;
SELECT c.idUsuario,c.comentario, f.idFoto, f.idUsuario
FROM fotos f
JOIN comentarios c
	ON c.idUsuario = 36
JOIN comentariosFotos cf
	ON cf.idFoto = f.idFoto
    AND c.idComentario = cf.idComentario
WHERE
	f.idUsuario = 11
    AND f.idFoto = 12
;