USE instagram_low_cost;
SELECT u.idUsuario, u.nombre AS nombreUsuario, COUNT(rf.idTipoReaccion) AS totalMeGusta
FROM usuarios u 
JOIN roles r 
    ON u.idRol = r.idRol 
JOIN reaccionesFotos rf
    ON u.idUsuario = rf.idUsuario
JOIN tiposReaccion tr
    ON rf.idTipoReaccion = tr.idTipoReaccion
WHERE r.descripcion = 'Administrador'
    AND tr.descripcion = 'Me gusta'
GROUP BY u.idUsuario HAVING totalMeGusta > 2;

SELECT *
	FROM roles r
JOIN usuarios u
	ON u.idRol = r.idRol
JOIN reaccionesFotos rf
	ON u.idUsuario = rf.idUsuario
JOIN tiposReaccion tr
	ON tr.idTipoReaccion = rf.idTipoReaccion
WHERE r.descripcion = "Usuario" AND tr.descripcion = "Me Gusta"

UNION

SELECT *
	FROM roles r
JOIN usuarios u
	ON u.idRol = r.idRol
JOIN reaccionesComentarios rc
	ON u.idUsuario = rc.idUsuario
JOIN tiposReaccion tr
	ON tr.idTipoReaccion = rc.idTipoReaccion
WHERE r.descripcion = "Usuario" AND tr.descripcion = "Me Gusta"

;
WHERE u.idUsuario = 4
ORDER BY u.idUsuario, rf.idTipoReaccion;
