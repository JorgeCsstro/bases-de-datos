# Reto 1: Consultas básicas

### Autor: Jorge Castro

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C1/reto1_sanitat?ref_type=heads

## Query 1
En esta query he realizado una consulta de la tabla `HOSPITAL` y seleccionaremos estas 3 columnas `HOSPITAL_COD`, `NOM`, y `TELEFON` para que solo nos muestren estas columnas.

(Opcional): He renombrado las columnas con `AS`.

```sql
SELECT HOSPITAL_COD AS "COD_HOSPITAL", NOM AS "NOMBRE", TELEFON AS "TELÉFONO"
FROM HOSPITAL;
```

## Query 2
En esta query he reutilizado el código de la Query1 y he añadido un substring en el cual coja de la columna `NOM` la seguna posicion solo un caracter y que sea la "a".

(Opcional): He renombrado las columnas con `AS`.

```sql
SELECT HOSPITAL_COD AS "COD_HOSPITAL", NOM AS "NOMBRE", TELEFON AS "TELÉFONO"
FROM HOSPITAL
WHERE SUBSTR(NOM,2,1) = "a";
```

## Query 3
En esta query he realizado una consulta de la tabla `PLANTILLA` y que me muestre el `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO` y `COGNOM`.

(Opcional): He renombrado las columnas con `AS`.

```sql
SELECT HOSPITAL_COD AS "COD_HOSPITAL", SALA_COD AS "COD_SALA", EMPLEAT_NO AS "NUM_EMP", COGNOM
FROM PLANTILLA;
```

## Query 4
En esta query he reutilizado el código de la Query3 y he añadido `WHERE TORN != "N";` para que en la columna `TORN` no esté la gente que tenga el turno "N" (Noche).

(Opcional): He renombrado las columnas con `AS`.

```sql
SELECT HOSPITAL_COD AS "COD_HOSPITAL", SALA_COD AS "COD_SALA", EMPLEAT_NO AS "NUM_EMP", COGNOM
FROM PLANTILLA
WHERE TORN != "N";
```

## Query 5
En esta query he seleccionado de la tabla `MALALT` toda la gente que haya nacido en el año 1960 con `YEAR`.

```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX) = 1960;
```

## Query 6
En esta query he reutilizado el código de la Query5 y en vez de poner un `=` he puesto un `>=` para que sean todos los que hayan nacido en 1960 o más.

```sql
SELECT *
FROM MALALT
WHERE YEAR(DATA_NAIX) >= 1960;
```
