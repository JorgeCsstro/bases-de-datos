-- QUERY12
-- Todas las facturas del cliente 1
SELECT SUM(Total) FROM Invoice
WHERE CustomerId = 1;

-- Clientes de cada empleado
SELECT CustomerId FROM Customer
WHERE SupportRepId = 3;

-- Total de las facturas de varios clientes
SELECT SUM(Total) FROM Invoice
WHERE CustomerId IN (1, 3, 12);

-- Total de las facturas de todos los clientes de un empleado
SELECT SUM(Total) FROM Invoice
WHERE CustomerId IN (
	SELECT CustomerId FROM Customer
    WHERE SupportRepId = 3
);

-- Datos de los empleados
SELECT EmployeeId, LastName, FirstName, (
	SELECT SUM(Total) FROM Invoice
	WHERE CustomerId IN (
		SELECT CustomerId FROM Customer
		WHERE Customer.SupportRepId = E.EmployeeId
	)
) AS TotalVentas
FROM Employee AS E;

-- Si tienes 2 mismas tablas que utilizar que sean la misma tabla, se ponen alias (AS) para diferenciarse