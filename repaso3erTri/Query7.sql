SELECT AlbumId, SUM(Milliseconds) AS DuracionTotal
From Track
JOIN Album
USING (AlbumId)
GROUP BY AlbumId
HAVING DuracionTotal > (
	SELECT AVG(DuracionTotal)
    FROM (
		SELECT AlbumId, SUM(Milliseconds) AS DuracionTotal
        From Track
        GROUP BY AlbumId
    )AS AlbumDuracion
);