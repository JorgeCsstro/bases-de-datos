SELECT P.PlaylistId, COUNT(*) AS N_Canciones
FROM Playlist P
JOIN PlaylistTrack PT USING(PlaylistId)
GROUP BY PlaylistId
LIMIT 1;