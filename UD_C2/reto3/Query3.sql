SELECT P.PlaylistId, P.Name, T.TrackId, T.Name, T.GenreId
FROM Playlist P
LEFT JOIN PlaylistTrack PT
	ON P.PlaylistId = PT.PlaylistId
LEFT JOIN Track T
	ON T.TrackId = PT.TrackId
WHERE T.TrackId IN (SELECT PT2.TrackId
					FROM PlaylistTrack PT2)
AND T.GenreId = 8;