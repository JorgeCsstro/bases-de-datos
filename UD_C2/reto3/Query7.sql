SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS Duracion_total,( 
				SELECT AVG(Tr.Milliseconds)
				FROM Track Tr
				) AS Duracion_media
FROM Album A
JOIN Track T USING(AlbumId)
GROUP BY A.AlbumId, A.Title
HAVING Duracion_total > Duracion_media;