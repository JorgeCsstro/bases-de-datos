SELECT G.GenreId, COUNT(*) AS Num_Canciones
FROM Genre G
JOIN Track T USING(GenreId)
GROUP BY GenreId
LIMIT 1;