SELECT C.CustomerId, C.FirstName, C.LastName, (
    SELECT SUM(UnitPrice * Quantity)
    FROM InvoiceLine IL
    WHERE IL.InvoiceId IN (
			SELECT InvoiceId
			FROM Invoice I
			WHERE I.CustomerId = C.CustomerId
			)
  ) AS Total
FROM Customer C
ORDER BY Total DESC;