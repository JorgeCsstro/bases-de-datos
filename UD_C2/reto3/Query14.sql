SELECT Ar.Name, A.Title 
FROM Artist Ar
JOIN Album A USING(ArtistId)
WHERE A.AlbumId = (
	SELECT MAX(Al.AlbumId)
	FROM Album Al
	WHERE Al.ArtistId = Ar.ArtistId
);