
-- Id y N canciones de cada album
SELECT AlbumId, COUNT(*) AS N_Canciones
FROM Track
GROUP BY AlbumId;

-- Media de canciones de todos los albumes
SELECT AVG(N_Canciones) FROM
(
	SELECT AlbumId, COUNT(*) AS N_Canciones
	FROM Track
	GROUP BY AlbumId
) AS Album_N_Canciones;

-- Consulta 1
SELECT AlbumId, COUNT(*) AS N_Canciones
FROM Track
GROUP BY AlbumId
HAVING N_Canciones > (
					SELECT AVG(N_Canciones) FROM
						(
							SELECT AlbumId, COUNT(*) AS N_Canciones
							FROM Track
							GROUP BY AlbumId
						) AS Album_N_Canciones
					);

-- Consulta con todo
SELECT *
FROM Album
WHERE AlbumId IN (
					SELECT AlbumId
					FROM Track
					GROUP BY AlbumId
					HAVING  COUNT(*) > (
										SELECT AVG(N_Canciones) FROM
											(
											SELECT AlbumId, COUNT(*) AS N_Canciones
											FROM Track
											GROUP BY AlbumId
											) AS Album_N_Canciones
										)
				);