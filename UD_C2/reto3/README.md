# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

## Autor: Jorge Castro

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C2/reto3?ref_type=heads

## Subconsultas Escalares (Scalar Subqueries) (De solo 1 dato)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa.

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Query 1
Obtener las canciones con una duración superior a la media.

```sql
SELECT T.TrackId, T.Milliseconds
FROM Track T
WHERE T.Milliseconds > (SELECT avg(Tr.Milliseconds)
						FROM Track Tr);
```

### Query 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

```sql
SELECT I.InvoiceId, C.CustomerId, I.InvoiceDate, C.Email
FROM Invoice I
JOIN Customer C
	ON I.CustomerId = C.CustomerId
WHERE C.Email = (SELECT Cu.Email
				 FROM Customer Cu
                 WHERE Cu.Email LIKE "emma_jones@hotmail.com")
ORDER BY I.InvoiceDate DESC
LIMIT 5;
```


## Subconsultas de varias filas (de varios datos)

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la Query principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`.

### Query 3
Mostrar las listas de reproducción en las que hay canciones de reggae.

```sql
SELECT P.PlaylistId, P.Name, T.TrackId, T.Name, T.GenreId
FROM Playlist P
LEFT JOIN PlaylistTrack PT
	ON P.PlaylistId = PT.PlaylistId
LEFT JOIN Track T
	ON T.TrackId = PT.TrackId
WHERE T.TrackId IN (SELECT PT2.TrackId
					FROM PlaylistTrack PT2)
AND T.GenreId = 8;
```

### Query 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

```sql
SELECT C.CustomerId, C.FirstName, C.LastName, I.Total
FROM Customer C
JOIN Invoice I
	ON C.CustomerId = I.CustomerId
WHERE I.Total IN (SELECT Inv.Total
				 FROM Invoice Inv
                 WHERE Inv.Total > 20.00);
```

### Query 5
Álbumes que tienen más de 15 canciones, junto a su artista.

```sql
SELECT *
FROM Album
JOIN Artist USING (ArtistId)
WHERE AlbumId IN (	SELECT AlbumId
					FROM Track
					GROUP BY AlbumId
					HAVING COUNT(*) > 15
);
```


### Query 6
Obtener los álbumes con un número de canciones superiores a la media.

```sql
SELECT *
FROM Album
WHERE AlbumId IN (
					SELECT AlbumId
					FROM Track
					GROUP BY AlbumId
					HAVING  COUNT(*) > (
										SELECT AVG(N_Canciones) FROM
											(
											SELECT AlbumId, COUNT(*) AS N_Canciones
											FROM Track
											GROUP BY AlbumId
											) AS Album_N_Canciones
										)
				);
```

### Query 7
Obtener los álbumes con una duración total superior a la media.

```sql
SELECT A.AlbumId, A.Title, SUM(T.Milliseconds) AS Duracion_total,( 
				SELECT AVG(Tr.Milliseconds)
				FROM Track Tr
				) AS Duracion_media
FROM Album A
JOIN Track T USING(AlbumId)
GROUP BY A.AlbumId, A.Title
HAVING Duracion_total > Duracion_media;
```

### Query 8
Canciones del género con más canciones.

```sql
SELECT G.GenreId, COUNT(*) AS Num_Canciones
FROM Genre G
JOIN Track T USING(GenreId)
GROUP BY GenreId
LIMIT 1;
```

### Query 9
Canciones de la _playlist_ con más canciones.

```sql
SELECT P.PlaylistId, COUNT(*) AS N_Canciones
FROM Playlist P
JOIN PlaylistTrack PT USING(PlaylistId)
GROUP BY PlaylistId
LIMIT 1;
```

## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la query externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la query externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento.

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

### Query 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

```sql
SELECT C.CustomerId, C.FirstName, C.LastName, (
    SELECT SUM(UnitPrice * Quantity)
    FROM InvoiceLine IL
    WHERE IL.InvoiceId IN (
			SELECT InvoiceId
			FROM Invoice I
			WHERE I.CustomerId = C.CustomerId
			)
  ) AS Total
FROM Customer C
ORDER BY Total DESC;
```

### Query 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

```sql
SELECT EmployeeId, FirstName, LastName, (
SELECT COUNT(*) AS N_Customers
    FROM Customer C
    WHERE C.SupportRepId = E.EmployeeId
    ) AS Num_Customers
FROM Employee E;
```

### Query 12
Ventas totales de cada empleado.

```sql
SELECT E.EmployeeID, E.FirstName, E.LastName, (
		SELECT SUM(Quantity)
		FROM InvoiceLine IL
		WHERE IL.InvoiceId IN (
					SELECT InvoiceId
					FROM Invoice I
					WHERE I.CustomerID IN (
							SELECT CustomerID
							FROM Customer C
							WHERE C.SupportRepId = E.EmployeeID
					)
		)
) AS Ventas
FROM Employee E
ORDER BY Ventas DESC;
```

### Query 13
Álbumes junto al número de canciones en cada uno.

```sql
SELECT A.AlbumId, A.Title,(
		SELECT COUNT(*)
		FROM Track T
		WHERE T.AlbumId = A.AlbumId
		) AS Num_Canciones
FROM Album A;
```

### Query 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

```sql
SELECT Ar.Name, A.Title 
FROM Artist Ar
JOIN Album A USING(ArtistId)
WHERE A.AlbumId = (
	SELECT MAX(Al.AlbumId)
	FROM Album Al
	WHERE Al.ArtistId = Ar.ArtistId
);
```