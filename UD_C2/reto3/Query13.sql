SELECT A.AlbumId, A.Title,(
		SELECT COUNT(*)
		FROM Track T
		WHERE T.AlbumId = A.AlbumId
		) AS Num_Canciones
FROM Album A;