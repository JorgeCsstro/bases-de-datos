SELECT T.TrackId, T.Milliseconds
FROM Track T
WHERE T.Milliseconds > (SELECT avg(Tr.Milliseconds)
						FROM Track Tr);