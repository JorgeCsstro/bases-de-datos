-- Albumes con mas de 15 canciones
SELECT AlbumId
FROM Track
GROUP BY AlbumId
HAVING COUNT(*) > 15;

-- Consulta en Where
SELECT *
FROM Album
JOIN Artist USING (ArtistId)
WHERE AlbumId IN (	SELECT AlbumId
					FROM Track
					GROUP BY AlbumId
					HAVING COUNT(*) > 15
);

-- Consulta con JOIN
SELECT *
FROM Album
JOIN Artist
	USING (ArtistId)
INNER JOIN (
		SELECT AlbumId, COUNT(*) AS N_Canciones
		FROM Track
		GROUP BY AlbumId
		HAVING N_Canciones > 15
) AS Album_N_Canciones
	USING (AlbumId);