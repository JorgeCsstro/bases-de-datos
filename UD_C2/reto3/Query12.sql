SELECT E.EmployeeID, E.FirstName, E.LastName, (
		SELECT SUM(Quantity)
		FROM InvoiceLine IL
		WHERE IL.InvoiceId IN (
					SELECT InvoiceId
					FROM Invoice I
					WHERE I.CustomerID IN (
							SELECT CustomerID
							FROM Customer C
							WHERE C.SupportRepId = E.EmployeeID
					)
		)
) AS Ventas
FROM Employee E
ORDER BY Ventas DESC;