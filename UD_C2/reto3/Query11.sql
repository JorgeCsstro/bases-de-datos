--  Sacar info de cada empleado (usando subconsultas correlacionales) (ESTA ESTA MEJOR)
SELECT EmployeeId, FirstName, LastName, (
SELECT COUNT(*) AS N_Customers
    FROM Customer C
    WHERE C.SupportRepId = E.EmployeeId
    ) AS Num_Customers
FROM Employee E;