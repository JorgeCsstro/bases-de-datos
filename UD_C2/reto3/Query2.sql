SELECT I.InvoiceId, C.CustomerId, I.InvoiceDate, C.Email
FROM Invoice I
JOIN Customer C
	ON I.CustomerId = C.CustomerId
WHERE C.Email = (SELECT Cu.Email
				 FROM Customer Cu
                 WHERE Cu.Email LIKE "emma_jones@hotmail.com")
ORDER BY I.InvoiceDate DESC
LIMIT 5;