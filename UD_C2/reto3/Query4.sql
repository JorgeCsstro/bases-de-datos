SELECT C.CustomerId, C.FirstName, C.LastName, I.Total
FROM Customer C
JOIN Invoice I
	ON C.CustomerId = I.CustomerId
WHERE I.Total IN (SELECT Inv.Total
				 FROM Invoice Inv
                 WHERE Inv.Total > 20.00);