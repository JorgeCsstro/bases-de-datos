# Reto 2.1: Consultas con funciones de agregación

### Autor: Jorge Castro

Para este reto, usaremos la base de datos `Chinook`, que podemos encontrar en el
siguiente repositorio: https://github.com/lerocha/chinook-database/

La base de datos Chinook simula una tienda de música en línea e incluye tablas
para artistas, álbumes, canciones, clientes, empleados, géneros musicales, pedidos
y facturas. Cada tabla contiene información relevante para su entidad respectiva,
como nombres de artistas, títulos de álbumes, géneros musicales, detalles de
pedidos y facturas, entre otros. Esta estructura permite practicar consultas SQL
relacionadas con la gestión de una tienda de música, desde la administración de
inventario hasta el análisis de ventas y clientes.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C2/reto1?ref_type=heads

## Query1
En esta Query he filtrado de la tabla `Customer` todos aquellos que en la columna `Country` tenga `France` en el

(Opcional): He puesto solo lo esencial del cliente (Id,FN,LN y Country)

```sql
USE Chinook;
SELECT C.CustomerId, C.FirstName, C.LastName, C.Country
	FROM Customer as C
WHERE Country LIKE "France";
```

## Query2
En esta Query he filtrado la tabla `Invoice` todas las facturas que esten entre el inicio de 2024 y el final del primer trimestre de este con esta sentencia `InvoiceDate >= '2024-01-01' AND InvoiceDate < '2024-04-01'`

(Opcional): He puesto solo lo esencial de `Invoice` y tambien lo he ordenado dependiendo de la fecha

```sql
USE Chinook;
SELECT I.InvoiceId, I.CustomerId, I.InvoiceDate
FROM 
    Invoice AS I
WHERE InvoiceDate >= '2024-01-01' AND InvoiceDate < '2024-04-01'
ORDER BY InvoiceDate;
```

## Query3
En esta Query he filtrado de la tabla `Track` todos aquellos que en la columna `Composer` tenga `AC/DC` en el

(Opcional): He puesto solo lo esencial de `Track` para este ejercicio

```sql
USE Chinook;
SELECT T.TrackId, T.Name, T.Composer
FROM 
	Track AS T
WHERE Composer LIKE "AC/DC";

```

## Query4
En esta Query he ordenado dependiendo de los `Bytes` de forma descendiente y despues de ordenarlos lo he limitado a 10 para que solo salgan los 10 que mas tienen

(Opcional): He puesto solo lo esencial de `Track` para este ejercicio

```sql
USE Chinook;
SELECT T.TrackId, T.Name, T.Bytes
FROM 
	Track AS T
ORDER BY Bytes DESC
LIMIT 10;
```

## Query5
En esta Query he añadido `DISTINCT` al `SELECT` para que no se repitan

(Opcional): He puesto solo lo esencial de `Customer` para este ejercicio y lo he ordenado de manera ascendente para mayor claridad

```sql
USE Chinook;
SELECT DISTINCT C.Country
FROM 
	Customer AS C
ORDER BY C.Country ASC;
```

## Query6
En esta Query he reutilizado lo mismo que la anterior, el `DISTINCT` para que no se repitan

(Opcional): He puesto solo lo esencial de `Genre` para este ejercicio y lo he ordenado de manera ascendente para mayor claridad

```sql
USE Chinook;
SELECT DISTINCT NAME
FROM 
	Genre
ORDER BY NAME ASC;
```

## Query7
En esta Query he juntado la tabla `Artist` y `Album` donde el `ArtistID` sea igual en ambas tablas, para esto he utilizado el `LEFT JOIN`

(Opcional): He puesto solo lo esencial de ambas tablas para este ejercicio y lo he ordenado para mayor claridad.

```sql
USE Chinook;
SELECT A.ArtistId, A.Name, AL.AlbumId, AL.Title
FROM 
    Artist AS A
LEFT JOIN Album AS AL
	ON A.ArtistId = AL.ArtistId
ORDER BY A.ArtistId;
```

## Query8
En esta Query he juntado la tabla `Employee` y `Employee`, porque tenemos que comparar dos datos de la misma tabla, donde el `E.ReportsTo` sea igual que `SU.EmployeeID`, para esto he utilizado el `LEFT JOIN`.

(Opcional): He puesto solo lo esencial de ambas tablas para este ejercicio y lo he ordenado para mayor claridad.

```sql
USE Chinook;
SELECT E.FirstName, E.LastName, SU.FirstName, SU.LastName
FROM 
	Employee AS E
LEFT JOIN Employee AS SU
	ON E.ReportsTo = SU.EmployeeId
ORDER BY E.BirthDate DESC
LIMIT 5;
```

## Query9
En esta Query he juntado la tabla `Customer` y `Invoice` donde el `CustomerID` sea igual en ambas tablas y que la ciudad del `Customer` sea `Berlin`, después he seleccionado los datos que pedia el ejercicio.

```sql
USE Chinook;
SELECT I.InvoiceDate, C.FirstName, C.LastName, I.BillingAddress, C.PostalCode, I.BillingCountry, I.Total
FROM
	Customer AS C
JOIN Invoice AS I
	ON C.CustomerId = I.CustomerId
	AND C.City LIKE "Berlin";
```

## Query10
En esta Query he juntado 4 tablas: `Playlist`, `Track`, `PlaylistTrack` y `Album`, donde `PT.PlaylistId` sea igaul a `P.PlaylistId` y `PT.TrackId` sea igual a `T.TrackId`, despues las filtro para que el nombre de la `Playlist` empieze por `C` y ordeno esta consulta por el `A.AlbumId` y `T.Milliseconds`.

(Opcional): He puesto solo lo esencial de las tablas para este ejercicio para mayor claridad.

```sql
USE Chinook;
SELECT P.Name, T.TrackId, T.Name, T.Milliseconds, A.AlbumId
FROM 
	Playlist AS P
JOIN Track AS T
JOIN PlaylistTrack AS PT
JOIN Album AS A
	ON PT.PlaylistId = P.PlaylistId
	AND PT.TrackId = T.TrackId
WHERE P.Name LIKE "C%"
ORDER BY A.AlbumId, T.Milliseconds DESC;
```

## Query11
En esta Query he juntado la tabla `Customer` y `Invoice` donde el `CustomerId` tiene que ser igual en ambas, despues los he filtrado para que solo me salgan todos aquellos que tengan un `I.Total` mayor a 10€ y finalmente lo he ordenado por el `C.LastName` de manera ascendente.

(Opcional): He puesto solo lo esencial de las tablas para este ejercicio para mayor claridad.

```sql
USE Chinook;
SELECT C.FirstName, C.LastName, I.Total
FROM
	Customer AS C
JOIN Invoice AS I
	ON C.CustomerId = I.CustomerID
WHERE I.Total > 10
ORDER BY C.LastName ASC;
```

## Query12
En esta Query he utilizado `MIN` (minimo), `AVG` (media) y `MAX` (maximo) para poder filtrar rápidamente los resultados de la columna `I.Total`.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
USE Chinook;
SELECT MIN(I.Total) AS min, AVG(I.Total) AS Med, MAX(I.Total) AS MAX
FROM
	Invoice AS I;
```

## Query13
En esta Query he utiliazado `COUNT`para contar todos los `ArtistId` de la tabla.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
USE Chinook;
SELECT COUNT(A.ArtistId) AS "Num Artistas"
FROM
	Artist AS A;
```

## Query14
En esta Query he juntado la tabla `Album` y `Track` donde el `AlbumId` sea igual en ambas tablas, despues los he filtrado para que el titulo tenga `Out Of Time` y finalmente he utiliazado `COUNT`para contar todos los `T.TrackId` de la tabla.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
USE Chinook;
SELECT COUNT(T.TrackId) AS "Num Tracks"
FROM
	Album AS A
JOIN Track AS T
	ON T.AlbumId = A.AlbumId
WHERE A.Title LIKE "Out Of Time";
```

## Query15
En esta Query he utiliazado `COUNT`para contar los `DISTINCT Country` de la tabla, he utilizado `DISTINCT` para que no cuente el mismo pais dos o mas veces.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
USE Chinook;
SELECT COUNT(DISTINCT C.Country) AS "Num Paises"
FROM
	Customer AS C;
```

## Query16
En esta Query he juntado la tabla `Track` y `Genre` donde el `GenreId` sea igual en ambas tablas y después he puesto cada genero con el numero de canciones que tiene cada uno.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
SELECT G.Name AS "Nom Genero", COUNT(G.GenreId) AS "Num Tracks"
FROM
	Track AS T
JOIN Genre AS G
ON T.GenreId = G.GenreId
GROUP BY G.GenreId;
```

## Query17
En esta Query he juntado la tabla `Album` y `Track` donde el `AlbumId` sea igual en ambas tablas, después he puesto cada `AlbumId` con el numero de canciones que tiene cada uno y finalmente los he ordenado por el numero de canciones que tiene cada uno.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
USE Chinook;
SELECT A.AlbumId, COUNT(T.TrackId) AS "Num Tracks"
FROM
	Album AS A
LEFT JOIN Track AS T
	ON T.AlbumId = A.AlbumId
GROUP BY A.AlbumId
ORDER BY COUNT(T.TrackId);
```

## Query18
En esta Query he juntado 3 tablas `Track`, `Genre` y `InvoiceLine`, donde `T.GenreId` sea igaul a `G.GenreId`, y `T.TrackId` sea igual a `IL.TrackId`, después he puesto el nombre del genero (`G.Name`) con el numero de "Compras" que tiene cada uno con `COUNT(IL.TrackId)`.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
SELECT G.Name, COUNT(IL.TrackId) AS "Num Compras"
FROM
	Track AS T
JOIN Genre AS G
    ON T.GenreId = G.GenreId
JOIN InvoiceLine AS IL
    ON T.TrackId = IL.TrackId
GROUP BY G.GenreId;
```

## Query19
En esta Query he juntado 3 tablas `Track`, `Album` y `InvoiceLine`, donde `T.AlbumId` sea igaul a `A.AlbumId`, y `T.TrackId` sea igual a `IL.TrackId`, después he puesto el nombre del album (`A.Title`) con el numero de "Compras" que tiene cada uno con `COUNT(IL.TrackId)` y finalmente los he ordenado por el numero de "Compras" que tiene cada uno de manera descendiente y limitado a 6.

(Opcional): He renombrado con `AS` para que se pueda leer mejor los resultados.

```sql
SELECT A.Title, COUNT(IL.TrackId) AS "Num Compras"
FROM
	Track AS T
JOIN Album AS A
    ON T.AlbumId = A.AlbumId
JOIN InvoiceLine AS IL
    ON T.TrackId = IL.TrackId
GROUP BY A.Title
ORDER BY COUNT(IL.TrackId) DESC
LIMIT 6;
```

## Query20
En esta Query he contado todos los `Country` que se repitan igual o mas de 5 veces, despues en el `SELECT` he utilizado el `DISTINCT` para que no se repitan a la hora de mostrarlos.

```sql
SELECT DISTINCT C.Country
FROM
	Customer AS C
GROUP BY C.Country
HAVING COUNT(C.Country) >= 5;
```