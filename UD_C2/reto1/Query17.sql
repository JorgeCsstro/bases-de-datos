USE Chinook;
SELECT A.AlbumId, COUNT(T.TrackId) AS "Num Tracks"
FROM
	Album AS A
LEFT JOIN Track AS T
	ON T.AlbumId = A.AlbumId
GROUP BY A.AlbumId
ORDER BY COUNT(T.TrackId);