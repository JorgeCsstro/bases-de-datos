USE Chinook;
SELECT C.FirstName, C.LastName, I.Total
FROM
	Customer AS C
JOIN Invoice AS I
	ON C.CustomerId = I.CustomerID
WHERE I.Total > 10
ORDER BY C.LastName ASC;