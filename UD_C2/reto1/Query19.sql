SELECT A.Title, COUNT(IL.TrackId) AS "Num Compras"
FROM
	Track AS T
JOIN Album AS A
ON T.AlbumId = A.AlbumId
JOIN InvoiceLine AS IL
ON T.TrackId = IL.TrackId
GROUP BY A.Title
ORDER BY COUNT(IL.TrackId) DESC
LIMIT 6;