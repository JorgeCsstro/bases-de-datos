USE Chinook;
SELECT T.TrackId, T.Name, T.Bytes
FROM 
	Track AS T
ORDER BY Bytes DESC
LIMIT 10;