USE Chinook;
SELECT *
FROM Invoice
WHERE QUARTER(InvoiceDate) = 1
AND year(InvoiceDate) = year(current_date());