USE Chinook;
SELECT A.ArtistId, A.Name, AL.AlbumId, AL.Title
FROM 
    Artist AS A
LEFT JOIN Album AS AL
	ON A.ArtistId = AL.ArtistId
ORDER BY A.ArtistId;