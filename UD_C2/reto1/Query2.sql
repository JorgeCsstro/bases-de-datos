USE Chinook;
SELECT I.InvoiceId, I.CustomerId, I.InvoiceDate
FROM 
    Invoice AS I
WHERE InvoiceDate >= '2024-01-01' AND InvoiceDate < '2024-04-01'
ORDER BY InvoiceDate;