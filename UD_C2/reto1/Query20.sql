SELECT DISTINCT C.Country
FROM
	Customer AS C
GROUP BY C.Country
HAVING COUNT(C.Country) >= 5;