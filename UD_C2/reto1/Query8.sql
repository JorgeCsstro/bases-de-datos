USE Chinook;
SELECT E.FirstName, E.LastName, SU.FirstName, SU.LastName
FROM 
	Employee AS E
LEFT JOIN Employee AS SU
	ON E.ReportsTo = SU.EmployeeId
ORDER BY E.BirthDate DESC
LIMIT 5;