USE Chinook;
SELECT COUNT(T.TrackId) AS "Num Tracks"
FROM
	Album AS A
JOIN Track AS T
	ON T.AlbumId = A.AlbumId
WHERE A.Title LIKE "Out Of Time";