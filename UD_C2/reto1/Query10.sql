USE Chinook;
SELECT P.Name, T.TrackId, T.Name, T.Milliseconds, A.AlbumId
FROM 
	Playlist AS P
JOIN Track AS T
JOIN PlaylistTrack AS PT
JOIN Album AS A
	ON PT.PlaylistId = P.PlaylistId
	AND PT.TrackId = T.TrackId
WHERE P.Name LIKE "C%"
ORDER BY A.AlbumId, T.Milliseconds DESC;