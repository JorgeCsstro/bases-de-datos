-- (El ON por lo que quieras filtrar) PD: No al 100%

-- LEFT JOIN: Mismo num de filas que la tabla de la LEFT (Con datos extra de RIGHT si tienen la misma)
-- (L Tbl: M, L, S)
-- (R Tbl: L, XS, XL)
-- (LEFT = M, L, S)

-- RIGHT JOIN: Mismo num de filas que la tabla de la RIGHT (Con datos extra de LEFT si tienen la misma)
-- (L Tbl: M, L, S)
-- (R Tbl: L, XS, XL)
-- (RIGHT = L, XS, XL)

-- INNER JOIN: Son aquellas que coinciden en ambas partes 
-- (L Tbl: M, L, S)
-- (R Tbl: L, XS, XL)
-- (INNER = L)

-- FULL JOIN: Coje todo lo de ambas tablas pero NO duplica si hay 2 que estan en las 2 tablas
-- (L Tbl: M, L, S)
-- (R Tbl: L, XS, XL)
-- (FULL = M, L, S, XS, XL)