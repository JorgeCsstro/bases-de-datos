USE Chinook;
SELECT I.InvoiceDate, C.FirstName, C.LastName, I.BillingAddress, C.PostalCode, I.BillingCountry, I.Total
FROM
	Customer AS C
JOIN Invoice AS I
	ON C.CustomerId = I.CustomerId
	AND C.City LIKE "Berlin";