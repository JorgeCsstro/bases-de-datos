USE Chinook;
SELECT CONCAT(E.FirstName, ", ", E.LastName) "EMPLEADO", CONCAT(SU.FirstName, ", ", SU.LastName) "SUPERVISOR"
FROM 
	Employee AS E
LEFT JOIN Employee AS SU
	ON E.ReportsTo = SU.EmployeeId
ORDER BY E.BirthDate DESC
LIMIT 5;