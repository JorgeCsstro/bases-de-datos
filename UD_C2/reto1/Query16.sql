SELECT G.Name AS "Nom Genero", COUNT(G.GenreId) AS "Num Tracks"
FROM
	Track AS T
JOIN Genre AS G
ON T.GenreId = G.GenreId
GROUP BY G.GenreId;