SELECT G.Name, COUNT(IL.TrackId) AS "Num Compras"
FROM
	Track AS T
JOIN Genre AS G
ON T.GenreId = G.GenreId
JOIN InvoiceLine AS IL
ON T.TrackId = IL.TrackId
GROUP BY G.GenreId;