SELECT *
FROM Album A
INNER JOIN Album Alb
ON Alb.AlbumId = A.AlbumId
WHERE A.ArtistId = (SELECT DISTINCT Al.ArtistId
					FROM Album Al
					WHERE Al.ArtistId = A.ArtistId
                    ORDER BY Al.AlbumId)
GROUP BY AlbumId