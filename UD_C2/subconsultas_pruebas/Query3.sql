SELECT Albumid, sum(T.Milliseconds) AS ntraks
	FROM Track T
GROUP BY Albumid
HAVING ntraks > (SELECT AVG(co.com)
				FROM (SELECT Albumid , sum(Tr.Milliseconds) AS com
						FROM Track Tr
						GROUP BY Albumid) AS co);