SELECT Albumid, count(*) AS ntraks
	FROM Track
GROUP BY Albumid
HAVING ntraks > (SELECT AVG(co.com)
				FROM (SELECT Albumid , count(*) AS com
						FROM Track
						GROUP BY Albumid) AS co);