SELECT *
FROM Track T
WHERE T.Milliseconds > (SELECT avg(Tr.Milliseconds)
						FROM Track Tr)
ORDER BY Milliseconds