SELECT T.AlbumId, count(T.TrackId) AS "Num Canciones"
FROM Track T
WHERE T.AlbumId = (SELECT A.AlbumId
					FROM Album A
					WHERE A.AlbumId = T.AlbumId)
GROUP BY AlbumId