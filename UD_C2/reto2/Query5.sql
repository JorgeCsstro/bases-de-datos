-- Query5
SELECT SU.FirstName, SU.LastName, E.FirstName, E.LastName
FROM Employee SU
JOIN Employee E
	ON SU.EmployeeId = E.ReportsTo
ORDER BY SU.EmployeeId ASC;