-- Query1
SELECT *
FROM Playlist P
WHERE P.PlaylistId IN (
	SELECT PT.PlaylistId
    FROM Track T
    JOIN PlaylistTrack PT
		ON PT.TrackId = T.TrackId
    ORDER BY T.AlbumId, T.UnitPrice ASC
    LIMIT 3
)
AND P.Name = "M%";