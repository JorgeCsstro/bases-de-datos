-- Cosas
SELECT DISTINCT SupportRepId
FROM Customer
WHERE SupportRepId IS NOT NULL;



SELECT TrackId
FROM InvoiceLine IL;

-- Canciones que se han comprado
SELECT Name, Composer
FROM Track 
WHERE TrackId IN
(
	SELECT TrackId
	FROM InvoiceLine IL
);

-- Canciones que NO se han comprado
SELECT Name, Composer
FROM Track 
WHERE TrackId NOT IN
(
	SELECT TrackId
	FROM InvoiceLine IL
);