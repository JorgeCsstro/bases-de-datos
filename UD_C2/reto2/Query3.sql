-- Query3
SELECT E.FirstName, E.LastName
FROM Employee E
WHERE E.EmployeeId IN (
	SELECT SupportRepId
    FROM Customer
);
