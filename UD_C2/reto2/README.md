# Reto 2.2: Consultas con subconsulatas

### Autor: Jorge Castro

Para este reto, usaremos la base de datos `Chinook`, que podemos encontrar en el
siguiente repositorio: https://github.com/lerocha/chinook-database/

La base de datos Chinook simula una tienda de música en línea e incluye tablas
para artistas, álbumes, canciones, clientes, empleados, géneros musicales, pedidos
y facturas. Cada tabla contiene información relevante para su entidad respectiva,
como nombres de artistas, títulos de álbumes, géneros musicales, detalles de
pedidos y facturas, entre otros. Esta estructura permite practicar consultas SQL
relacionadas con la gestión de una tienda de música, desde la administración de
inventario hasta el análisis de ventas y clientes.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/JorgeCsstro/bases-de-datos/-/tree/main/UD_C2/reto2?ref_type=heads

## Query1

```sql
SELECT *
FROM Playlist P
WHERE P.PlaylistId IN (
	SELECT PT.PlaylistId
    FROM Track T
    JOIN PlaylistTrack PT
		ON PT.TrackId = T.TrackId
    ORDER BY T.AlbumId, T.UnitPrice ASC
    LIMIT 3
)
AND P.Name = "M%";
```

## Query2

```sql
SELECT DISTINCT Ar.ArtistId, Ar.Name
FROM Artist Ar
JOIN Album Al
	ON Al.ArtistId = Ar.ArtistId
WHERE Al.AlbumId IN (
	SELECT T.AlbumId
    FROM Track T
    WHERE (T.Milliseconds/60000) > 5
);
```

## Query3

```sql
SELECT E.FirstName, E.LastName
FROM Employee E
WHERE E.EmployeeId IN (
	SELECT SupportRepId
    FROM Customer
);
```

## Query4

```sql
SELECT Name, Composer
FROM Track 
WHERE TrackId NOT IN
(
	SELECT TrackId
	FROM InvoiceLine IL
);
```

## Query5

```sql
SELECT SU.FirstName, SU.LastName, E.FirstName, E.LastName
FROM Employee SU
JOIN Employee E
	ON SU.EmployeeId = E.ReportsTo
ORDER BY SU.EmployeeId ASC;
```

## Query6

```sql
SELECT TrackId, Name, Composer
FROM Track
WHERE TrackId IN 
(
	SELECT TrackId
    FROM InvoiceLine
    WHERE InvoiceId IN
    (
		SELECT InvoiceId
        FROM Invoice
        WHERE CustomerId IN
		(
			SELECT CustomerId
            FROM Customer
            WHERE FirstName LIKE "Luis"
            AND LastName LIKE "Rojas"
        )
    )
);
```

## Query7

```sql
SELECT DISTINCT T.AlbumId, T.TrackId, T.Name
FROM Track T
WHERE T.AlbumId IN
(
	SELECT T.AlbumId
    FROM InvoiceLine IL
    JOIN Track T
		ON T.TrackId = IL.TrackId
	ORDER BY IL.UnitPrice
)

-- No se como limitar los Albumes
```

## Query8

```sql
SELECT *
FROM Album
WHERE ArtistId =
(
	SELECT ArtistId
    FROM Artist
    WHERE Name = "Queen"
);
```