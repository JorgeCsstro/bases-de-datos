-- Query7
SELECT DISTINCT T.AlbumId, T.TrackId, T.Name
FROM Track T
WHERE T.AlbumId IN
(
	SELECT T.AlbumId
    FROM InvoiceLine IL
    JOIN Track T
		ON T.TrackId = IL.TrackId
	ORDER BY IL.UnitPrice
)

-- No se como limitar los Albumes