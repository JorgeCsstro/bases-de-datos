-- Query2
SELECT DISTINCT Ar.ArtistId, Ar.Name
FROM Artist Ar
JOIN Album Al
	ON Al.ArtistId = Ar.ArtistId
WHERE Al.AlbumId IN (
	SELECT T.AlbumId
    FROM Track T
    WHERE (T.Milliseconds/60000) > 5
);