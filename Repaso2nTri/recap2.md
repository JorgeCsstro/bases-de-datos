[Enlace a mi repositorio](https://gitlab.com/luciferma14/bbdd/-/tree/main)
 
 "Javier Puertas me ha ayudado a saber como poner el GitLab y como hacer el README"


# EJERCICIOS

# Ejercicio 1
```sql
    SELECT idFoto
    FROM fotos,usuarios
    WHERE usuarios.idUsuario = fotos.idUsuario
    AND fotos.idUsuario = 36;
```
Realizo una consulta para averiguar las fotos del usuario 36.
Hago un SELECT del id de las fotos (idFoto), de las tablas fotos y usuarios, seguido de un WHERE donde igualo el id del usuario (idUsuaio) de la tabla usuarios, con el id del usuario de la tabla fotos. Y termino con AND en el que el id del usuario de la tabla fotos sea igual el número 36.

# Ejercicio 2
```sql
    SELECT *
    FROM fotos
    WHERE YEAR(fechaCreacion) = "2024"
        AND MONTH(fechaCreacion) = "1"
        AND fotos.idUsuario = 36;
```       
Otra forma
```sql
    SELECT *
    FROM fotos
    WHERE fotos.idUsuario = 36
        AND (fotos.fechaCreacion > "2023/12/31/"
        AND fotos.fechaCreacion < "2024/02/01/");
``` 

Realizo una consulta para averiguar las fotos del usuario 36 tomadas en el mes de Enero.
Explicaré la primera forma. Hago un SELECT de todos los datos (*) de la tabla fotos, seguido de un WHERE donde, con la función YEAR MONTH, pongo los datos que necesito para realizar la consulta. Y por último, con el AND, digo que el id del usuario de la tabla fotos sea igual el número 36.

# Ejercicio 3
```sql
    SELECT usuarios.nombre, comentarios.comentario, fotos.url
    FROM comentarios, usuarios, fotos, comentariosFotos
    WHERE fotos.idUsuario = 11
        AND usuarios.idUsuario = 36
        AND usuarios.idUsuario = comentarios.idUsuario
        AND comentariosFotos.idFoto = fotos.idFoto
        AND comentarios.idComentario = comentariosFotos.idComentario;
```
Realizo una consulta para averiguar los comentarios del usuario 11 sobre la foto 11.
Hago un SELECT para coger el nombre de la tabla usuarios, los comentarios de la tabla comentarios y la url de la tabla fotos. Todos estos datos los recojo de las tablas: comentarios, usuarios, fotos y comentariosFotos. En el WHERE le digo que el id del usuario de la tabla fotos sea igual a 11, y en los siguientes ANDs comparo las tablas entre sí, para que se cumplan la relación.

# Ejercicio 4
```sql
    SELECT fotos.idFoto,fotos.descripcion, tiposReaccion.descripcion
    FROM reaccionesFotos, fotos, tiposReaccion
    WHERE reaccionesFotos.idUsuario = 11
        AND reaccionesFotos.idFoto = fotos.idFoto
        AND reaccionesFotos.idTipoReaccion = 4
        AND tiposReaccion.idTipoReaccion = reaccionesFotos.idTipoReaccion;
```
Realizo una consulta para averiguar las fotos que han sorprendido al usuario 11.
Hago un SELECT para coger el id de la fotos, la descripción de las fotos y la descripcion de la tabla de tipos de reacciones, para saber cual es el tipo de reacción. En el WHERE digo que el id del usuario de la tabla de reacciones de las fotos sea igual a 11, y en los siguientes ANDs comparo las tablas entre sí, para que se cumplan la relación.

# Ejercicio 7 
```sql
    SELECT COUNT(descripcion) as "Descripción"
    FROM fotos
    WHERE descripcion LIKE "%playa%";
```
Realizo una consulta para averiguar el número de fotos tomadas en la playa (en base a la descripción de la foto).
Hago un SELECT con la función COUNT para que me muestre el número absoluto sobre las fotos que tienen como descripción incluida la palabra playa.
