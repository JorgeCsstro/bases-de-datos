# Reto 2: Consultas Empresa y Videoclub

Álvaro Escartí - Estudiante 1º DAW

En este reto trabajamos con las base de datos `Videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/escartii/bases-de-datos/

<div align="center">

## Empresa

</div>

## Query 1

Contar cuantos clientes hay
```sql
-- Contar cuantos clientes hay
SELECT COUNT(*) FROM client;
```

## Query 2
Media 
``` sql
-- Sacar la media de la fila codiFactura de la tabla Factura
SELECT AVG(codiFactura) FROM Factura;
```

## Query 3
Número Máximo
```sql
-- Sacar el número más grande de la fila codiFactura de la tabla Factura
SELECT MAX(codiFactura) FROM Factura;
```

## Query 4
Número Mínimo
```sql
-- Sacar el número más pequeño de la fila codiFactura de la tabla Factura
SELECT MIN(codiFactura) FROM Factura;
```


## Query 5
Substring
```sql
-- Cortar cadenas de texto, el primer número donde queremos a empezar a cortar
-- el segundo número es a partir donde queremos parar 
SELECT SUBSTRING(Nom,1,3) FROM actor;

```

## Query 6
Ordenar Descendente
```sql
SELECT DNI,Import
FROM Factura
ORDER BY Import DESC;
```

## Query 7
Ordenar Ascendente
```sql
SELECT DNI,Import
FROM Factura
ORDER BY Import ASC;
```

## Query 8
Limitar la salida
```sql
SELECT titol FROM pelicula
LIMIT 3;
```

## Query 9
Actualizar los datos
```sql
UPDATE ACTOR
SET Nom = "Jordi"
WHERE CodiActor = 5;
```

## Query 10
Insertar datos a una tabla
```sql
INSERT INTO ACTOR
VALUES
(8, "Paco");
```

## Query 11
Quitar valores de una tabla
```sql
DELETE FROM ACTOR
WHERE CodiActor = 15;
```
Otro ejemplo
```sql
DELETE FROM ACTOR
WHERE CodiActor IN(9,10);
```

Otro ejemplo
```sql
DELETE FROM ACTOR
WHERE CodiActor BETWEEN 9 AND 10;
```

Otro ejemplo

```sql
DELETE FROM ACTOR
WHERE CodiActor > 11;
```

## Query 12
Distinct es para que solo te muestre 1 vez cada uno
```sql
SELECT DISTINCT OFICI
FROM EMP;
```

## Query 13
Sacar el campo si es nulo
```sql
SELECT *
FROM EMP
WHERE COMISSIO IS NULL;
```

## Como exportar e importar la base de datos

Server > Data Export > Export to Self-Contained File > Start Export
## Como importar la base de datos
Server > Data Import > Seleccionamos la BDD 

