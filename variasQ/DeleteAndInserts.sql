SELECT *
FROM ACTOR;

INSERT INTO ACTOR
VALUES
(8, "Paco");

INSERT INTO ACTOR
-- Especifica el orden de los datos como los añades
(Nom, CodiActor)
VALUES
("Ximo", 6);

INSERT INTO ACTOR
(CodiActor)
VALUES
(15);

-- Para quitar valores de la tabla
DELETE FROM ACTOR
WHERE CodiActor = 15;

-- Para poner varios
DELETE FROM ACTOR
WHERE CodiActor IN(9,10);

DELETE FROM ACTOR
WHERE CodiActor BETWEEN 9 AND 10;

DELETE FROM ACTOR
WHERE CodiActor > 11;