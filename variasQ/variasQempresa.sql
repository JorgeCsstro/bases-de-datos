-- No me muestra los NULL
SELECT *
FROM EMP
WHERE COMISSIO >= 0;

-- Me muestra los NULL
SELECT *
FROM EMP
WHERE COMISSIO IS NULL;

-- Me muestra los 2 primeros de commissio
SELECT *
FROM EMP
ORDER BY COMISSIO DESC
LIMIT 2;

-- Me muestra los 2 primeros de salario
SELECT *
FROM EMP
ORDER BY SALARI DESC
LIMIT 2;

-- Distinct es para que solo te muestre 1 vez cada uno
SELECT DISTINCT OFICI
FROM EMP;

SELECT count(DISTINCT OFICI)
FROM EMP;
